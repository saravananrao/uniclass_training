import uuid


def get_hexid_from_uuid():
    return str(uuid.uuid4().hex)
