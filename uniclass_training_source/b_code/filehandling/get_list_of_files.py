from os import listdir
from os.path import isfile, join

def get_list_of_files_from_directory(str_dirname: str):

    filenameslist=[fname for fname in listdir(str_dirname)\
                   if isfile(join(str_dirname, fname))]

    return filenameslist


