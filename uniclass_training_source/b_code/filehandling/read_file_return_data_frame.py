import pandas


def read_file(src_file_path: str):

    dframe = 0

    if src_file_path.rfind(".") > -1:

        if src_file_path[src_file_path.rfind(".") + 1:].upper().strip() == "CSV":
            dframe = pandas.read_csv(src_file_path)
        if src_file_path[src_file_path.rfind(".") + 1:].upper().strip() == "XLSX":
            dframe = pandas.read_excel(src_file_path)

    return dframe


# def write_file(dframe: object,
#                des_file_path: str,
#                des_file_name: str,
#                des_file_type: str
#                ):
#     full_file_name = des_file_path + "\\" + des_file_name + "." + des_file_type
#
#     if des_file_type.upper() == "CSV":
#         dframe.to_csv(full_file_name, header=True, index=False)
