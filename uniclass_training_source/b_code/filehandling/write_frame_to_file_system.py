def write_file(dframe: object,
               des_file_path: str,
               des_file_name: str,
               des_file_type: str
               ):
    result = bool(False)
    full_file_name = des_file_path + "\\" + des_file_name + "." + des_file_type
    if des_file_type.upper() == "CSV":
        dframe.to_csv(full_file_name, header=True, index=False)
        result = bool(True)
    return result



