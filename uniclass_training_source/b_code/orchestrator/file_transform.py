from uniclass_training_source.b_code.dataframemanipulation.new_column_in_dataframe \
    import insert_identity_column_value_in_dataframe
from uniclass_training_source.b_code.filehandling.get_list_of_files \
    import get_list_of_files_from_directory
from uniclass_training_source.b_code.filehandling.read_file_return_data_frame \
    import read_file
from uniclass_training_source.b_code.filehandling.write_frame_to_file_system \
    import write_file


def file_transform_action(folder_name: str):

    list_of_filenames = \
        get_list_of_files_from_directory(str_dirname=folder_name)

    for filename \
            in list_of_filenames:
        df = read_file(src_file_path=folder_name + filename)

        df = insert_identity_column_value_in_dataframe(dframe=df,
                                                       column_name="Unique ID",
                                                       column_index=0,
                                                       default_value="0")
        target_file_path = folder_name + "Transformed\\"

        result = write_file(dframe=df,
                            des_file_path=target_file_path,
                            des_file_name=filename,
                            des_file_type="csv")

        print(filename + " transformed.") if result else print(filename + " not transformed.")
