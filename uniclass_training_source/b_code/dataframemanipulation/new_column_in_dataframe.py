from uniclass_training_source.b_code.uniqueidentites.generate_random_identities \
    import get_hexid_from_uuid


def insert_identity_column_value_in_dataframe(dframe: object,
                                              column_name: str,
                                              column_index: int,
                                              default_value: object
                                              ):
    dframe.insert(column_index, column_name, default_value, False)

    for index \
            in range(len(dframe)):
        dframe.loc[index, column_name] = get_hexid_from_uuid()

    return dframe
