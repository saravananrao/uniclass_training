# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
# import sys
# sys.path.append('C:\\Users\\Saravanan.S.Rao\\PythonDev\\code\\uniclass_training\\')

from uniclass_training_source.b_code.dataframemanipulation.new_column_in_dataframe \
    import insert_identity_column_value_in_dataframe
from uniclass_training_source.b_code.filehandling.get_list_of_files \
    import get_list_of_files_from_directory
from uniclass_training_source.b_code.filehandling.read_file_return_data_frame \
    import read_file
from uniclass_training_source.b_code.filehandling.write_frame_to_file_system \
    import write_file


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # print_hi('PyCharm')

    directory_name = \
        "C:\\Users\\Saravanan.S.Rao\\Documents\\PythonPractices\\Load_4_input_tables\\"

    list_of_filenames = \
        get_list_of_files_from_directory(str_dirname=directory_name)

    for filename \
            in list_of_filenames:
        df = read_file(src_file_path=directory_name + filename)

        df = insert_identity_column_value_in_dataframe(dframe=df,
                                                       column_name="Unique ID",
                                                       column_index=0,
                                                       default_value="0")

        target_file_path = directory_name + "Transformed\\"

        result = write_file(dframe=df,
                            des_file_path=target_file_path,
                            des_file_name=filename,
                            des_file_type="csv")

        print(filename + " transformed.") if result else print(filename + " not transformed.")

    # See PyCharm help at https://www.jetbrains.com/help/pycharm/ 94449 25541
